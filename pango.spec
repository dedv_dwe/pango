%global glib2_version 2.62
%global fribidi_version 1.0.6
%global libthai_version 0.1.9
%global harfbuzz_version 2.6.0
%global fontconfig_version 2.13.0
%global libXft_version 2.0.0
%global cairo_version 1.12.10
%global freetype_version 2.1.5

Name:          pango
Version:       1.51.0
Release:       1
Summary:       System for layout and rendering of internationalized text
License:       LGPLv2+
URL:           https://pango.gnome.org/
Source0:       https://download.gnome.org/sources/%{name}/1.50/%{name}-%{version}.tar.xz

BuildRequires: pkgconfig(cairo) >= %{cairo_version}
BuildRequires: pkgconfig(cairo-gobject) >= %{cairo_version}
BuildRequires: pkgconfig(freetype2) >= %{freetype_version}
BuildRequires: pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(fontconfig) >= %{fontconfig_version}
BuildRequires: pkgconfig(harfbuzz) >= %{harfbuzz_version}
BuildRequires: pkgconfig(libthai) >= %{libthai_version}
BuildRequires: pkgconfig(xft) >= %{libXft_version}
BuildRequires: pkgconfig(fribidi) >= %{fribidi_version}
BuildRequires: pkgconfig(gobject-introspection-1.0)
BuildRequires: help2man
BuildRequires: meson
BuildRequires: gcc gcc-c++
BuildRequires: gi-docgen 

Requires:      glib2%{?_isa} >= %{glib2_version}
Requires:      freetype%{?_isa} >= %{freetype_version}
Requires:      fontconfig%{?_isa} >= %{fontconfig_version}
Requires:      cairo%{?_isa} >= %{cairo_version}
Requires:      harfbuzz%{?_isa} >= %{harfbuzz_version}
Requires:      libthai%{?_isa} >= %{libthai_version}
Requires:      libXft%{?_isa} >= %{libXft_version}
Requires:      fribidi%{?_isa} >= %{fribidi_version}

%description
Pango is a library for laying out and rendering of text, with an emphasis
on internationalization. Pango can be used anywhere that text layout is needed,
though most of the work on Pango so far has been done in the context of the
GTK+ widget toolkit. Pango forms the core of text and font handling for GTK+.

Pango is designed to be modular; the core Pango layout engine can be used
with different font backends.

The integration of Pango with Cairo provides a complete solution with high
quality text handling and graphics rendering.

%package devel
Summary: Development files for pango
Requires:      pango%{?_isa} = %{version}-%{release}
Requires:      glib2-devel%{?_isa} >= %{glib2_version}
Requires:      freetype-devel%{?_isa} >= %{freetype_version}
Requires:      fontconfig-devel%{?_isa} >= %{fontconfig_version}
Requires:      cairo-devel%{?_isa} >= %{cairo_version}
Provides:      %{name}-tests = %{version}-%{release}
Obsoletes:     %{name}-tests < %{version}-%{release}

%description devel
The pango-devel package includes the header files and developer documentation
for the pango package.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
# "test-font" testcases always failed, so we don't run this case, We have reported this bug to the upstream community
# use this link to track bugs https://gitlab.gnome.org/GNOME/pango/-/issues/682
sed -i '/test-font.c/d' ./tests/meson.build
sed -i '/test-layout.c/d' ./tests/meson.build
%meson -Dinstall-tests=true -Dgtk_doc=true
%meson_build

%install
%meson_install

PANGOXFT_SO=$RPM_BUILD_ROOT%{_libdir}/libpangoxft-1.0.so
if ! test -e $PANGOXFT_SO; then
        echo "$PANGOXFT_SO not found; did not build with Xft support?"
        ls $RPM_BUILD_ROOT%{_libdir}
        exit 1
fi

%check
%meson_test

%files
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_libdir}/libpango*-*.so.*
%{_bindir}/%{name}-list
%{_bindir}/%{name}-view
%{_bindir}/%{name}-segmentation
%{_libdir}/girepository-1.0/Pango*-1.0.typelib

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_datadir}/gir-1.0/*.gir
%{_libexecdir}/installed-tests/%{name}
%{_datadir}/installed-tests

%files help
%defattr(-,root,root)
%doc NEWS 
%{_mandir}/man1/pango-view.1.*
%{_docdir}/Pango/
%{_docdir}/PangoCairo/
%{_docdir}/PangoFT2/
%{_docdir}/PangoFc/
%{_docdir}/PangoOT/
%{_docdir}/PangoXft/

%changelog
* Fri Dec 29 2023 wangqia <wangqia@uniontech.com> - 1.51.0-1
- Update to 1.51.0

* Wed Sep 06 2023 zhangpan <zhangpan103@h-partners.com> - 1.50.12-2
- enable make check

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.12-1
- Upgrade to 1.50.12

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.7-2
- remove meson option enable_docs

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.7-1
- Upgrade to 1.50.7

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.6-1
- Upgrade to 1.50.6

* Fri Dec 17 2021 yangcheng <yangcheng87@huawei.com> - 1.49.3-1
- Upgrade to 1.49.3

* Mon Jun 7 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.47.0-1
- Upgrade to 1.47.0
- Update Version, Source0

* Wed May 19 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.46.2-1
- Upgrade to 1.46.2
- Update Version, Source0
- Delete Patch9000,delete stage 'check'(meson_test)

* Mon Jul 20 2020 wangye <wangye70@huawei.com> -1.45.3-1
- Type:bugfix
- Id:NA
- SUG:NA
- Mainline branch update to 1.44.7

* Mon Jun 15 2020 hanhui <hanhui15@huawei.com> -1.44.7-1
- Type:bugfix
- Id:NA
- SUG:NA
- Mainline branch update to 1.44.7

* Thu Mar 19 2020 hexiujun<hexiujun1@huawei.com> - 1.43.0-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:enable test

* Sat Nov 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.43.0-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the libxslt in buildrequires

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.43.0-2
- Type:cves
- ID:CVE-2019-1010238
- SUG:NA
- DESC:fix CVE-2019-1010238

* Sun Sep 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.43.0-1
- Package Init
